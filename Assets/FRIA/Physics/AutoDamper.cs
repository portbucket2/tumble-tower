﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(OverlapDetection))]
public class AutoDamper : MonoBehaviour
{
    public OverlapDetection ovDetect;
    public float retainRate = 0.65f;
    public bool shouldDamp = true;
    public bool shouldDampRotation = false;
    private void Start()
    {
        if (!ovDetect) ovDetect = this.GetComponent<OverlapDetection>();
    }
    private void FixedUpdate()
    {
        if (!shouldDamp) return;
        foreach (var item in ovDetect.items)
        {

            Vector3 v = item.attachedRigidbody.velocity.normalized * item.attachedRigidbody.velocity.magnitude * retainRate;
            //Debug.LogFormat("{0} to {1}",em.rgbd.velocity,v);
            item.attachedRigidbody.velocity = v;

            item.attachedRigidbody.AddForce(v - item.attachedRigidbody.velocity, ForceMode.VelocityChange);

            if (shouldDampRotation)
            {
                item.attachedRigidbody.angularVelocity = item.attachedRigidbody.angularVelocity.normalized * item.attachedRigidbody.angularVelocity.magnitude * retainRate;
            }
        }
    }
}
