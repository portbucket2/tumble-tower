using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneInputController : MonoBehaviour
{
    public static PlaneInputController Instance;
    public Camera cam;
    public Plane inputPlane;
    private void Awake()
    {
        Instance = this;
        inputPlane.SetNormalAndPosition(this.transform.forward, this.transform.position);
    }
    static Vector3 rpoint;
    public static Vector3 GetInputPointOnPlane()
    {
        Ray r = Instance.cam.ScreenPointToRay(Input.mousePosition);
        float dist;
        if (Instance.inputPlane.Raycast(r, out dist))
        {
            rpoint = r.GetPoint(dist);
            Instance.transform.position = rpoint;
            return rpoint;
        }
        
        return Vector3.zero;
    }

   

}
