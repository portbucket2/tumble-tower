using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PotatoSDK;
//using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;
#if UNITY_EDITOR
using LionStudios.Suite.Debugging;
#endif

public static class AnalyticsAssistant
{
    static bool lionInitialized = false;
    public static void EnsureInit()
    {
        if (lionInitialized) return;
        lionInitialized = true;
#if UNITY_EDITOR
        LionDebugger.Hide();
#endif
        LionAnalytics.GameStart();
    }
    public static void LevelStarted(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            Debug.LogError("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        LionAnalytics.LevelStart(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelNumber.ToString());
    }
    public static void LevelCompleted(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            Debug.LogError("Analytics call made before Potato is ready!");
            return;
        }

        EnsureInit();
        LionAnalytics.LevelComplete(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelNumber.ToString());
    }
    public static void LevelFailed(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            Debug.LogError("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        LionAnalytics.LevelFail(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelNumber.ToString());
    }
    public static void LevelRestart(int levelNumber)
    {
        if (!Potato.IsReady)
        {
            Debug.LogError("Analytics call made before Potato is ready!");
            return;
        }
        EnsureInit();
        LionAnalytics.LevelRestart(levelNumber, 1);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus., levelNumber.ToString());
    }
}
