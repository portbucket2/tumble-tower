﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace FRIA
{
    public class BaseEditor : Editor
    {


        public void DrawDefaultInspectorGUI()
        {
            base.OnInspectorGUI();
        }
        //public void LoadType()
        //{
        //    ICustomEditor ice = this as ICustomEditor;
        //    if (ice != null)
        //    {
        //        string[] guids = AssetDatabase.FindAssets(string.Format("{0} t:Script", ice.ScriptTitle));
        //        string path = AssetDatabase.GUIDToAssetPath(guids[0]);
        //        Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
        //        EditorGUI.BeginDisabledGroup(true);
        //        EditorGUILayout.ObjectField("Script:", obj, typeof(Object), false);
        //        EditorGUI.EndDisabledGroup();
        //    }
        //}
        public void DrawScriptTitle()
        {
            ICustomEditor ice = this as ICustomEditor;
            if (ice != null)
            {
                string[] guids = AssetDatabase.FindAssets(string.Format("{0} t:Script", ice.ScriptTitle));
                string path = AssetDatabase.GUIDToAssetPath(guids[0]);
                Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField("Script:", obj, typeof(Object), false);
                EditorGUI.EndDisabledGroup();
            }
        }
        public void DrawEditorTitle()
        {
            ICustomEditor ice = this as ICustomEditor;
            if (ice != null)
            {

                string[] guids = AssetDatabase.FindAssets(string.Format("{0} t:Script", ice.EditorTitle));
                string path = AssetDatabase.GUIDToAssetPath(guids[0]);
                Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField("Editor:", obj, typeof(Object), false);
                EditorGUI.EndDisabledGroup();
            }
        }

        public override void OnInspectorGUI()
        {
            OnCustomInspectorGUI();
        }

        public virtual void OnCustomInspectorGUI()
        {
            DrawDefaultInspectorGUI();
            DrawEditorTitle();
        }

        public void Button(string title, System.Action act, float height = 25)
        {
            if (GUILayout.Button(title,GUILayout.Height(height)))
            {
                act?.Invoke();
            }
        }
    }
    interface ICustomEditor
    {
        string ScriptTitle { get; }
        string EditorTitle { get; }
        //System.Type scriptType { get; }
    }
}


#endif