using System.Collections;
using PotatoSDK;
#if UNITY_EDITOR
using LionStudios.Suite.Debugging;
#endif

public class LevelMan : GameLevelManager
{
    public int MAX_GAME_SCENES = 5;
    public bool isSplashScreen;
    public override int GetBuildIndexForLevelIndex(int levelIndex)
    {
        return (levelIndex%MAX_GAME_SCENES)  + 1;
    }

    private IEnumerator Start()
    {
        if (isSplashScreen)
        {
            while (!Potato.IsReady)
            {
#if UNITY_EDITOR
                if (LionDebugger.IsShowing()) LionDebugger.Hide();
#endif
                yield return null;
            }
#if UNITY_EDITOR
            if (LionDebugger.IsShowing()) LionDebugger.Hide();
#endif
            LoadLatest();
        }
    }
}
