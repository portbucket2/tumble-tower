using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionCamTexController : MonoBehaviour
{
    public static RenderTexture rendTex2D;
    public Camera reflectionCam;
    public RenderTexture currenTex;
    public Material mat;


    // Start is called before the first frame update
    void Awake()
    {
        if (!rendTex2D)
        {
            if (currenTex.height == Screen.height && currenTex.width == Screen.width)
            {
                rendTex2D = currenTex;
            }
            else
            {
                rendTex2D = new RenderTexture(Screen.width, Screen.height, 16);
                currenTex = rendTex2D;
            }
            mat.SetTexture("ReflectTex", rendTex2D);
        }
        reflectionCam.targetTexture = rendTex2D;
    }

}
