using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvasMan : MonoBehaviour
{
    public static GameCanvasMan Instance { get; private set; }
    public BarUI stressBar;
    public GameObject nextDisplay;
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

        stressBar.colorLogic = SimulationManager.BarColorLogic;
        stressBar.LoadValue(0, 100, true);
        SimulationManager.Instance.onMaxStressUpdated += (float s) =>
        {
            stressBar.LoadValue(s, 100);
            //stressText.text = string.Format("Max Stress: {0}",s);
        };
    }
    public void TurnOff()
    {
        gameObject.SetActive(false);
    }
}
