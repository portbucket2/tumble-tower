using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseController : MonoBehaviour
{
    [Range(0, 360)]
    public float phaseShift = 180;
    internal List<PhaseController> subPhaseController = new List<PhaseController>();

    private void Start()
    {
        OnStart();
    }
    public void Connect(PhaseController basePhaseController)
    {
        if(basePhaseController)basePhaseController.subPhaseController.Add(this);
        OnConnect(basePhaseController);
    }

    public void ApplyPhase(float phase, float ampM)
    {
        phase += phaseShift;
        OnApplyPhase(phase, ampM);
        foreach (PhaseController item in subPhaseController)
        {
            item.ApplyPhase(phase, ampM);
        }
    }
    public void Break()
    {
        OnBreak();
        foreach (PhaseController item in subPhaseController)
        {
            item.Break();
        }
    }
    protected virtual void OnConnect(PhaseController basePhaseController) { }
    protected virtual void OnStart() { }
    protected virtual void OnApplyPhase(float phase, float ampM) { }
    protected virtual void OnBreak() { }
}
