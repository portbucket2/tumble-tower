using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueueManager : MonoBehaviour
{
    public bool disableErrorGain = false;
    public Text tempErrorText;
    public float inputSensitivity = 0.5f;
    public List<AttachmentController> nextItems;
    public int simulateOn = 0;
    public AttachmentController lastItem;

    DragReporter dragReporter;

    //float totalError = 0;
    AttachmentController currentItem;
    PhaseController firstPhaseController;
    float iMult;
    void Start()
    {
        firstPhaseController = lastItem.GetComponent<PhaseController>();
        iMult = Screen.height / 2880.0f;
        currentConnection = lastItem.FetchNextConnection();
        dragReporter = GetComponent<DragReporter>();
        dragReporter.onDrag_delta += OnDragDelta;
        dragReporter.onDragEnd += OnDragEnd;
        dragReporter.onDragStart += OnDragStart;
        PlaceInGame();
    }
    TopConnection currentConnection;
    void PlaceInGame()
    {
        currentItem = nextItems[0];
        nextItems.RemoveAt(0);
        if (nextItems.Count > 0)
        {
            Vector3 pos = nextItems[0].transform.position;
            pos.x = NextOnQueueMan.Instance.transform.position.x;
            nextItems[0].transform.position = pos;
        }
        else
        {
            GameCanvasMan.Instance.nextDisplay.SetActive(false);
        }
        currentItem.PlaceInGame(currentConnection, prevController: lastItem);
        currentItem.onSnapSuccess += AttachNow;
    }
    void OnDragStart(Vector2 vInp)
    {
        if (currentItem)
        {
            currentItem.OnDragStart(vInp);
        }
    }
    void OnDragDelta(Vector2 vInp)
    {
        if (currentItem)
        {
            currentItem.OnDragInput(vInp*inputSensitivity/iMult);
        }
    }
    void OnDragEnd(Vector2 vInp)
    {
        if (currentItem)
        {
            currentItem.OnDragEnd();
        }
    }

    void AttachNow()
    {
        //float newPossibleError = totalError + error;
        
        //if (!disableErrorGain && (newPossibleError * totalError > 0))
        //{
        //    totalError += error;
        //    totalError *= 1.5f;
        //}
        //else
        //{
        //    totalError += error;
        //}

        //tempErrorText.text = string.Format("Error: {0:F2}%", totalError * 100);
        if (lastItem.IsOutOfConnections)
        {
            lastItem = currentItem;
        }
        currentItem = null;
        if (simulateOn >= 0 && nextItems.Count == simulateOn)
        {
            SimulationManager.Instance.StartSimulation(firstPhaseController);
        }
        else
        {
            currentConnection = lastItem.FetchNextConnection();
            PlaceInGame();
        }
    }

}
