using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingDisc : PhaseController
{
    public Transform mover;
    public Transform disc;
    public float discRad = 0.2f;
    public float maxRollRange= 0.5f;
    public Rigidbody rgbd;
    Quaternion initialRotation;
    protected override void OnConnect(PhaseController basePhaseController)
    {
        initialRotation = disc.localRotation;
    }
    protected override void OnApplyPhase(float phase, float ampM)
    {
        float distance = maxRollRange* ampM * Mathf.Sin(phase);
        mover.localPosition = new Vector3(distance,0,0);

        float angle = distance*180 / ( Mathf.PI * discRad);
        disc.localRotation = initialRotation * Quaternion.Euler(0, -angle, 0);
    }

    protected override void OnBreak()
    {
        rgbd.isKinematic = false;
        rgbd.transform.SetParent(null);
    }
}
