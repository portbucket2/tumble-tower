using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StressReciever : MonoBehaviour
{
    public static StressReciever lastPlacedStressReciever;
    
    public static System.Action onStressUpdated;
    public Vector3 sumOfScaledStressVectors;
    public float MaxSelfStress;


    public float Stress { get { return Mathf.RoundToInt(sumOfScaledStressVectors.magnitude); } }

    void Start()
    {
        SimulationManager.allStressRecievers.Add(this);
        sumOfScaledStressVectors = Vector3.zero;
    }


    Vector3 pivot;
    float range;

    public void OnPlacement(Vector3 anchor, Vector3 centre, float range)
    {
        lastPlacedStressReciever = this;
        this.range = range;
        this.pivot = anchor;
        Vector3 weightVector = centre - pivot;
        sumOfScaledStressVectors = MaxSelfStress * weightVector / range;
        //Debug.LogFormat("total: {0}", sumOfScaledStressVectors);
        onStressUpdated?.Invoke();
    }
    public void OnWeightAdded(Vector3 weightPoint, float weightMag)
    {
        Vector3 weightVector = weightPoint - pivot;
        Vector3 newStressV = MaxSelfStress * weightMag * weightVector / range;


        //Debug.LogFormat("Stressful: {0}", newStressV);
        sumOfScaledStressVectors += newStressV;
        //Debug.LogFormat("total: {0}", sumOfScaledStressVectors);

        onStressUpdated?.Invoke();

    }


}
