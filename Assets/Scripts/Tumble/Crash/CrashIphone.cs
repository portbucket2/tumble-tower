using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashIphone : CrashEffect
{
    public Texture2D crashTex;
    public Renderer mrend;

    Material m;
    private void Start()
    {
        m = Instantiate(mrend.material);
        mrend.material = m;
    }
    protected override void OnCrash()
    {
        m.SetTexture("_BaseMap", crashTex);
        //Debug.Log("Base");
    }

}
