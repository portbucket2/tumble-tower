using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashEffect : MonoBehaviour
{
    public bool crashCompleted = false;
    // Start is called before the first frame update

    public void CrashNow()
    {
        if (crashCompleted) return;
        OnCrash();
        crashCompleted = true;
    }
    protected virtual void OnCrash()
    {
        
    }
}
