using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashTrigger : MonoBehaviour
{
    
    private void OnCollisionEnter(Collision collision)
    {
        CrashEffect ce = collision.gameObject.GetComponent<CrashEffect>();
        if (ce && !ce.crashCompleted)
        {
            ce.CrashNow();
        }
    }
}
