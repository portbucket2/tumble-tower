using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tumbler : PhaseController
{
    public float maxAngle;
    public Vector3 axis;
    public Rigidbody rgbd;
    Quaternion initialRotation;
    float angle;


    protected override void OnStart()
    {
        if (!rgbd.gameObject.activeSelf)
        {
            Debug.LogError("RGBD sleeping");
        }
        else
        {
            rgbd.useGravity = true;
            rgbd.isKinematic = true;
        }
    }
    protected override void OnConnect(PhaseController basePhaseController)
    {
        initialRotation = this.transform.rotation;
    }

    protected override void OnApplyPhase(float phase, float ampM)
    {
        float targetAngle = ampM * maxAngle * Mathf.Sin(phase);
        angle = Mathf.Lerp(angle, targetAngle, 2 * Time.deltaTime);

        this.transform.rotation = initialRotation * Quaternion.Euler(angle * axis);
    }

    protected override void OnBreak()
    {
        rgbd.isKinematic = false;
        rgbd.transform.SetParent(null);
    }
}


