using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopConnection : MonoBehaviour
{
    public float range;
    public MeshRenderer rend;
    Material m;
    Color noHighLightColor;
    Color baseColor;
    public Color readyColor = Color.green;
    public Color readyHighColor = Color.green;
    public Color targetColor1 = Color.red;
    public Color targetColorHighlight = Color.red;
    public float freq=4;
    [Range(0,1)]
    public float danceRange = 1;
    private void Start()
    {
        noHighLightColor = new Color(.2f,.2f,.2f,1);
        if (!rend) rend = GetComponent<MeshRenderer>();
        m = Instantiate(rend.material);
        rend.material = m;
        SetVisiblity(false);
        baseColor = m.color;
    }
    bool isReady;
    public void SetReady(bool isReady)
    {
        this.isReady = isReady;
    }
    public void SetVisiblity(bool active)
    {
        rend.enabled = active;
    }

    private void Update()
    {
        if (rend.enabled)
        {
            if (isReady)
            {
                m.SetColor("_EmissionColor", readyHighColor);
                m.SetColor("_BaseColor", readyColor);
            }
            else
            {
                //m.SetColor("_EmissionColor", Color.red);
                //m.SetColor("_BaseColor", Color.red);
                float f = ((Mathf.Sin(Time.time * freq) + 1)*danceRange / 2) + (1-danceRange);
                Color c1 = Color.Lerp(noHighLightColor,targetColorHighlight, f);
                Color c2 = Color.Lerp(baseColor,targetColor1, f);
                m.SetColor("_EmissionColor", c1);
                m.SetColor("_BaseColor", c2);
            }

        }
    }
}
