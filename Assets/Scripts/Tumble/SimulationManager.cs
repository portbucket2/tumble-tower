using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour
{
    public System.Action simulationStarted;
    public System.Action simulationBroken;
    public System.Action<float> onMaxStressUpdated;
    public static List<StressReciever> allStressRecievers = new List<StressReciever>();
    public static SimulationManager Instance { get; private set; }
    public float f =0.2f;
    PhaseController firstPhaseController;
    public float ampGainTime =2;
    // Start is called before the first frame update
    bool isSimulationActive;
    public SimulationType simType;
    private void Awake()
    {
        Instance = this;
        allStressRecievers.Clear();

        StressReciever.onStressUpdated += OnStressUpdated;
    }
    private void OnDestroy()
    {
        StressReciever.onStressUpdated -= OnStressUpdated;
    }

    float maxStress = 0;
    void OnStressUpdated()
    {
        maxStress = 0;
        foreach (var item in allStressRecievers)
        {
            if (item.Stress > maxStress)
            {
                maxStress = item.Stress;
            }
        }
        onMaxStressUpdated?.Invoke(maxStress);
    }

    public void StartSimulation(PhaseController firstPhaseController)
    {
        FRIA.Centralizer.Add_DelayedMonoAct(this, GameCanvasMan.Instance.TurnOff,1f);
        this.firstPhaseController = firstPhaseController;
        firstPhaseController.Connect(null);
        //error = Mathf.Abs(error);
        if (maxStress <= (int) SimulationType.Stable)
        {
            simType = SimulationType.Stable;
        }
        else if (maxStress <= (int)SimulationType.SemiStable)
        {
            simType = SimulationType.SemiStable;
        }
        else if (maxStress < (int)SimulationType.Fail)
        {
            simType = SimulationType.Fail;
        }
        else 
        {
            simType = SimulationType.Crash;
        }
        isSimulationActive = true;
        simulationStarted?.Invoke();
    }
    public static Color BarColorLogic(float f)
    {
        f = f * 100;
        if (f <= (float)SimulationType.Stable)
        {
            return Color.green;
        }
        else if (f <= (float)SimulationType.SemiStable)
        {
            float p = (f - (float)SimulationType.Stable) / ((float)SimulationType.SemiStable - (float)SimulationType.Stable);
            return Color.Lerp(Color.green,Color.yellow,p);
        }
        else if (f < (int)SimulationType.Fail)
        {
            float p = (f - (float)SimulationType.SemiStable) / ((float)SimulationType.Fail - (float)SimulationType.SemiStable);
            return Color.Lerp(Color.yellow, Color.red, p);
        }
        else
        {
            return Color.red;
        }
    }

    IEnumerator Start()
    {
        while (!isSimulationActive)
        {
            yield return null;
        }

        switch (simType)
        {
            case SimulationType.Stable:
                yield return StartCoroutine(RunStable(f));
                break;
            case SimulationType.SemiStable:
                yield return StartCoroutine(RunSemiStable(f));
                break;
            case SimulationType.Fail:
                yield return StartCoroutine(RunFail(f));
                break;
            case SimulationType.Crash:
                yield return StartCoroutine(RunCrash(f));
                break;
        }


    }
    float starttime;
    float elapsedTime;
    float cycleTime;
    float ampM;
    float ampCap;
    void PerFrame(float ampPerSec)
    {
        elapsedTime = Time.time - starttime;
        ampM += ampPerSec * Time.deltaTime;
        ampM = Mathf.Clamp(ampM,0,ampCap);
        firstPhaseController.ApplyPhase(2 * Mathf.PI * f * elapsedTime, ampM);
    }
    IEnumerator RunCycle(float ampPerSec)
    {
        starttime = Time.time;
        elapsedTime = 0;
        while (elapsedTime <= cycleTime)
        {
            PerFrame(ampPerSec);
            yield return null;
        }
    }
    //IEnumerator RunCotCycle(float ampPerSec)
    //{
    //    starttime = Time.time;
    //    elapsedTime = 0;
    //    while (elapsedTime <= cycleTime/2)
    //    {
    //        PerFrame(ampPerSec);
    //        yield return null;
    //    }
    //}
    IEnumerator RunFailCycle(float breakAngle)
    {
        starttime = Time.time;
        elapsedTime = 0;
        breakAngle = breakAngle*Mathf.PI / 180;
        float angle = 0;
        while (angle <= breakAngle)
        {
            elapsedTime = Time.time - starttime;
            angle = 2 * Mathf.PI * f * elapsedTime;
            firstPhaseController.ApplyPhase(angle, ampM);
            yield return null;
        }
    }
    IEnumerator RunStable(float freq)
    {
        f = freq;
        cycleTime = 1 / f;
        ampM = 0;
        ampCap = 0.80f;
        yield return StartCoroutine(RunCycle(1.75f/cycleTime));
        yield return StartCoroutine(RunCycle(-0.41f / cycleTime));
        yield return StartCoroutine(RunCycle(-0.41f / cycleTime));
        UIMan.Instance.ShowSuccess();
    }
    IEnumerator RunSemiStable(float freq)
    {
        f = freq;
        cycleTime = 1 / f;
        ampM = 0;
        ampCap = 1;
        yield return StartCoroutine(RunCycle(1 / cycleTime));
        yield return StartCoroutine(RunCycle(0));
        yield return StartCoroutine(RunCycle(-1 / cycleTime));
        UIMan.Instance.ShowSuccess();
    }

    IEnumerator RunFail(float freq)
    {
        f = freq;
        cycleTime = 1 / f;
        ampM = 0;
        ampCap = 1;
        yield return StartCoroutine(RunCycle(1 / cycleTime));
        yield return StartCoroutine(RunCycle(0));
        float breakAngle = (Random.Range(0, 2) == 0) ? 90 : 270;
        yield return StartCoroutine(RunFailCycle(breakAngle));
        firstPhaseController.Break();
        simulationBroken?.Invoke();
        UIMan.Instance.ShowFail();
    }
    IEnumerator RunCrash(float freq)
    {
        f = freq;
        cycleTime = 1 / f;
        ampM = 1;
        ampCap = 1;
        yield return StartCoroutine(RunFailCycle(90));
        firstPhaseController.Break();
        simulationBroken?.Invoke();
        UIMan.Instance.ShowFail();
    }
}

public enum SimulationType
{
    Stable = 25,
    SemiStable = 75,
    Fail = 100,
    Crash = 1000,
}