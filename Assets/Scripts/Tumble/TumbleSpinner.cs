using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumbleSpinner : MonoBehaviour
{
    public Vector3 spinRate = new Vector3(0, 1, 0);

    private void Start()
    {
        SimulationManager.Instance.simulationStarted += OnSim;
        SimulationManager.Instance.simulationBroken += OnBreak;
        targetM = 0;
    }
    public float m;
    float targetM;
    void OnSim()
    {
        targetM = 1;
    }
    void OnBreak()
    {
        targetM = 0;
    }
    private void Update()
    {
        m = Mathf.Lerp(m,targetM,1*Time.deltaTime);
        transform.Rotate(m*spinRate * Time.deltaTime, Space.Self);
    }
}
