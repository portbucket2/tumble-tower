using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AttachmentController : MonoBehaviour
{

    public static float YMAX = 5;
    public static float XMAX = 2.5f;

    PhaseController phaseController;
    public const float SNAP_R = 0.25f;

    //public float spawnHeight = 2;
    public float weight = 0.25f;
    public Transform bodyItem;
    public float selfAttachRange = 0;
    public List<TopConnection> topConnections;
    public Action onSnapSuccess;

    public StressReciever stressReciever;
    public Transform selfPlacementPoint
    {
        get
        {
            return this.transform;
        }
    }
    //public Transform nextPlacementPoint;

    //public float nextAttachRange = 0;
    public bool IsOutOfConnections { get { return topConnections.Count <= 0; } }
    public TopConnection FetchNextConnection()
    {
        TopConnection con = topConnections[0];
        topConnections.RemoveAt(0);
        return con;
    }

    static bool inputSet = false;
    private void Start()
    {
        if (!inputSet)
        {
            float equivalentWidth = 1440.0f * Screen.height / 2880.0f; 
            XMAX = XMAX * Screen.width / equivalentWidth;
            inputSet = true;
        }

        phaseController = GetComponent<PhaseController>();
    }

    public PlacementPlane pPlane;

    
    public void PlaceInGame(TopConnection connection,AttachmentController prevController)
    {
        stressReciever = GetComponent<StressReciever>();
        pPlane = CreatePlacementPlane(connection,prevController);
        selfPlacementPoint.position = pPlane.startPoint;
        selfPlacementPoint.rotation = pPlane.StartRotation;
    }
    private PlacementPlane CreatePlacementPlane(TopConnection connection ,AttachmentController prevController)
    {
        PlacementPlane p = new PlacementPlane();

        p.attachPoint = connection.transform.position;
        
        p.attachRange = connection.range + selfAttachRange;



        if (connection.range > 0)
        {
            p.xDir = connection.transform.right;
        }
        else
        {
            p.xDir = selfPlacementPoint.right;
        }

        Vector3 x0P = p.attachPoint;
        x0P.x = 0;
        Vector3 x0V = p.attachPoint - x0P;
        //Debug.DrawRay(p.attachPoint,p.xDir, Color.red, 3);
        //Debug.DrawLine(x0P,p.attachPoint,Color.cyan,3);
        float m1 = x0V.magnitude;
        float theta = Vector3.Angle(x0V,p.xDir);
        
        if (theta <= 90)
        {
            float mag = m1 / Mathf.Cos(theta*Mathf.PI/180.0f);
            //Debug.LogFormat("m1 {0}, mag {1}, theta {2}, cos {3}",m1,mag,theta,Mathf.Cos(theta));
            p.startPoint = p.attachPoint - p.xDir * mag;
        }
        else
        {
            float mag = m1 / Mathf.Cos((180-theta)*Mathf.PI / 180.0f);
            p.startPoint = p.attachPoint + p.xDir * mag;
        }


        p.startPoint.y = Mathf.Min(p.attachPoint.y + 3.2f,4);
        p.connection = connection;
        p.prevCon = prevController;

        PlaneInputController.Instance.inputPlane.SetNormalAndPosition(p.Forward, p.attachPoint);
        return p;
    }

    Vector3 GetAdjustedPointX(Vector3 p, Vector3 pointOnPlane)
    {
        Vector3 finalPoint;
        Vector3 x0V = pointOnPlane - p;
        float m1 = x0V.magnitude;
        float theta = Vector3.Angle(x0V, pPlane.xDir);
        if (theta <= 90)
        {
            float mag = m1 / Mathf.Cos(theta * Mathf.PI / 180.0f);
            //Debug.LogFormat("m1 {0}, mag {1}, theta {2}, cos {3}",m1,mag,theta,Mathf.Cos(theta));
            finalPoint = pointOnPlane - pPlane.xDir * mag;
        }
        else
        {
            float mag = m1 / Mathf.Cos((180 - theta) * Mathf.PI / 180.0f);
            finalPoint = pointOnPlane + pPlane.xDir * mag;
        }
        return finalPoint;
    }

    Vector3 initialOffset;
    public void OnDragStart(Vector2 input)
    {
        initialOffset = selfPlacementPoint.position - PlaneInputController.GetInputPointOnPlane();
    }

    public void OnDragInput(Vector2 input)
    {
       // PlaneInputController.GetInputPointOnPlane();
        pPlane.connection.SetVisiblity(true);
        //Vector3 inp = pPlane.xDir * input.x + new Vector3(0, input.y, 0);
        //selfPlacementPoint.position += inp * Time.deltaTime;
        Vector3 pos = PlaneInputController.GetInputPointOnPlane() + initialOffset;
        Vector3 finalPos = pos;
        if (pos.x > XMAX)
        {
            finalPos.x = XMAX;
            finalPos = GetAdjustedPointX(finalPos, pos);
        }
        else if (pos.x<-XMAX)
        {
            finalPos.x = -XMAX;
            finalPos = GetAdjustedPointX(finalPos, pos);
        }

        if (finalPos.y > YMAX)
        {
            finalPos.y = YMAX;
        }
        else if (finalPos.y < pPlane.attachPoint.y-0.02f)
        {
            finalPos.y = pPlane.attachPoint.y - 0.02f;
        }
        selfPlacementPoint.position = finalPos;
        //if (pos.x < XMAX && pos.x > -XMAX && pos.y < YMAX && pos.y >= pPlane.attachPoint.y)
        //{
        //    selfPlacementPoint.position = pos;
        //}
        if (CanSnapNow())
        {
            pPlane.connection.SetReady(true);
        }
        else
        {
            pPlane.connection.SetReady(false);
        }
    }
    public void OnDragEnd()
    {
        if (CanSnapNow())
        {
            SnapIt();
        }
    }

    public bool CanSnapNow()
    {
        Vector3 posVec = (selfPlacementPoint.position - pPlane.attachPoint);
        if (pPlane.attachRange>0)
        {
            Vector3 cross = Vector3.Cross(posVec, pPlane.xDir);
            float mX = cross.magnitude;
            float mD = Vector3.Dot(posVec, pPlane.xDir);
            if (mX < SNAP_R)
            {
                if (Mathf.Abs(mD) < SNAP_R + pPlane.attachRange)
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return posVec.magnitude < SNAP_R;
        }
    }

    public void SnapIt()
    {
        pPlane.connection.SetVisiblity(false);
        Vector3 posVec = (selfPlacementPoint.position - pPlane.attachPoint);
        //float error = 0;
        if (pPlane.attachRange > 0)
        {
            float mD = Vector3.Dot(posVec, pPlane.xDir);
            mD = Mathf.Clamp(mD, -pPlane.attachRange, pPlane.attachRange);
            //error =  mD/pPlane.attachRange;
            selfPlacementPoint.position = pPlane.attachPoint + pPlane.xDir * mD;
            if (selfAttachRange > 0)
            {
                bodyItem.SetParent(null);
                selfPlacementPoint.position = pPlane.attachPoint;
                bodyItem.SetParent(selfPlacementPoint);
            }
        }
        else
        {
            selfPlacementPoint.position = pPlane.attachPoint ;
        }
        this.transform.SetParent(pPlane.prevCon.transform);
        this.phaseController.Connect(pPlane.prevCon.phaseController);
        if (stressReciever) stressReciever.OnPlacement(this.transform.position, bodyItem.transform.position, selfAttachRange);
        else if (StressReciever.lastPlacedStressReciever) StressReciever.lastPlacedStressReciever.OnWeightAdded(this.transform.position,weight);
        onSnapSuccess?.Invoke();
    }


}
#if UNITY_EDITOR
[CustomEditor(typeof(AttachmentController))]
public class AttachmentControllerEditor : Editor
{

    private AttachmentController c;

    public void OnSceneGUI()
    {
        c = this.target as AttachmentController;
        PlacementPlane pp = c.pPlane;
        if (pp != null)
        {
            Handles.color = new Color(1,1,0,0.5f);
            Handles.DrawWireDisc(pp.startPoint, pp.Forward, 2);
            Handles.color = new Color(0, 1, 0, 0.5f);
            Handles.DrawWireDisc(pp.attachPoint, pp.Forward, 3);
        }
    }
        
}
#endif

public class PlacementPlane
{
    public Vector3 startPoint;

    private Vector3 _xdir;
    public Vector3 xDir { get { return _xdir; } set { _xdir = value.normalized; } }

    public Vector3 attachPoint;
    public float attachRange;
    public AttachmentController prevCon;
    public TopConnection connection;
    public Vector3 Forward
    {
        get
        {
            return Vector3.Cross(xDir,Vector3.up);
        }
    }

    public Quaternion StartRotation
    {
        get
        {
            return Quaternion.LookRotation(Forward, Vector3.up);
        }
    }
}
