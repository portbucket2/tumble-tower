using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextOnQueueMan : MonoBehaviour
{
    public static NextOnQueueMan Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
}
